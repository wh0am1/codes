(function(DDD){
	DDD.webos = {
		aosDeskindex : 0,
		aos_timer : undefined,
		moveDir : 1,
		deskArr : [],
		intervalID : 0,
		flag : false,
		mMousePos : {},
		trashTempApp : [],

		dockIconState : 0,
		init : function()
		{
			var This = this;
			this.setArea();

			this.setElement();
			this.setDockIcon();
			$(window).resize(function(){
				This.setArea();
				This.setElement();
			});
			this.initEvent();
		},
		
		test : function(jq)
		{
			var index = this.aosDeskindex;
		//	alert(jq.parent().html());
			var imgSrc = jq.parent().find('a img').attr('src');
			var title = jq.prev().text();
			var ul = $('.iconDesk').eq(index).find('ul');
			
			var li = $("<li class='aos_icon'><div class='iconHover'></div><img src=" + imgSrc + " /><div class='title'>" + title + "</div></li>").prependTo(ul);

			this.addAppEvent();
		},
		
		addAppEvent : function()
		{
			$('.iconDesk').find('li').mouseover(function(){
				$(this).find('.iconHover').show();	
			}).mouseout(function(){
				$(this).find('.iconHover').hide();	
			}).dblclick(function(e){
				var that = this;
				$('#showAppWin').window('open');
			}).draggable({ 
				revert : "invalid",
				helper:function(event){
					var imgSrc = $(this).find('img').attr('src');
					var tmp = $('<div class="helper"><img src="' + imgSrc + '" style="width:70px; height:70px"/></div>').appendTo('body');
					return tmp; 
				}
			}).disableSelection();

			$('#aos_manager').unbind().dblclick(function(){
				$('#addAppWin').window({
					title : 'Add apps',
					cache : false,
					href : 'addAppWin.html'
				});
				
				$('#addAppWin').window('open');
			});
		},

		initEvent : function()
		{
			var This = this;
			$(document).mousemove(function(e){
				$('#aos_screen').hide();
				clearInterval(This.intervalID);
				This.setArrow(e);
			}).mouseover(function(e){
				//var left = e.pageY;
				//var top = e.pageX;
				//this.intervalID = setInterval('AOS.webos.raphaelClock(e.pageY, e.pageX)', 6000, e);
				//this.intervalID = setInterval('AOS.webos.raphaelClock()', 6000, e);
			});
			
			//this.intervalID = setInterval(this.raphaelClock, 6000);
			
			$('.aos_icon').bind('contextmenu', function(e){
				e.stopPropagation();
				e.preventDefault();
				$('#iconMenu').menu('show', {
						left : e.pageX,
						top : e.pageY
				});
			});

			$('.arrowLeft, .arrowRight').click(function(e){
				 This.aosDeskindex += parseInt($(this).attr('movedir'));
				 This.toggleDesk();
			});
			
			$('.aos_desk').each(function(i){
				$(this).css({'background-image' : 'url(./image/page'+i+'.jpg)'});	
			});
			
			$('.iconDesk').find('li').mouseover(function(){
				$(this).find('.iconHover').show();	
			}).mouseout(function(){
				$(this).find('.iconHover').hide();	
			}).dblclick(function(e){
				var that = this;
				$('#showAppWin').window('open');
				
				/*
				$('#showAppWin').window({
					title : 'cuteEditor',
					cache : false,
					href : 'test.html'
				});
				$('#showAppWin').window('open');
			
				$('#showAppWin').window('open');
				$('#showAppWin').panel({
					onMinimize : function()
					{
						This.addWorkIcon($(that).find('img').clone());
					}
				});
				*/
			}).draggable({ 
				revert : "invalid",
				helper:function(event){
					var imgSrc = $(this).find('img').attr('src');
					var tmp = $('<div class="helper"><img src="' + imgSrc + '" style="width:70px; height:70px"/></div>').appendTo('body');
					return tmp; 
				}
			}).disableSelection();
			
			$('#aos_manager').unbind().dblclick(function(){
				$('#addAppWin').window({
					title : 'Add apps',
					cache : false,
					href : 'addAppWin.html'
				});
				
				$('#addAppWin').window('open');
			});

			setInterval(function(){
				$('#aos_timer').html(This.currentTime);
			},1000);

			$('.aos_trash').droppable({
				accept : '.iconDesk li',
				greedy : true,
				tolerance : 'fit',
				drop : function(e, ui)
				{
					$.messager.confirm('message', 'Remove this app?', function(r){
						if (r)
						{
							var dom = ui.draggable;
							var tempDom = $(dom).clone(true);
						
							$(tempDom).attr('whichDesk', This.aosDeskindex);
							This.trashTempApp.push(tempDom);
							//alert($(dom).html());
							This.deleteIcon(ui.draggable[0]);
						}
					});
				}
			}).bind('contextmenu', function(e){
				e.stopPropagation();
				e.preventDefault();
				$('#trashMenu').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			}).dblclick(function(){
				var that = this;
				$('#showTrashWin').window('open');
				
				$('#showTrashWin').panel({
					onMinimize : function()
					{
						This.addWorkTrashIcon($(that).find('img').clone());
					}
				});
				
				var tempAppUl = $('#showTrashWin ul');
				var trashButton = $('#showTrashWin').find('.trashButton');
				var recoverButton = $('#showTrashWin').find('.recoverButton');
				tempAppUl.children().remove();
				for (var i=0; i<This.trashTempApp.length; i++)
				{
					This.trashTempApp[i].appendTo(tempAppUl);
				}
					
				tempAppUl.find('img').width(50).height(50);
				tempAppUl.find('li').css('float', 'left');
				tempAppUl.find('li').click(function(e){
					e.stopPropagation();
					if ($(this).hasClass('trashAppSelected'))
					{
						$(this).removeClass('trashAppSelected');
						$(this).find('img').css('opacity', 1);	
					}
					else
					{
						$(this).addClass('trashAppSelected');
						$(this).find('img').css('opacity', 0.5);
					}
				});
				
				trashButton.click(function(){
					var tt = $('#showTrashWin li').size();
					if ($('#showTrashWin li').size() == 0) return false;
					$.messager.confirm('message', 'Remove all apps?', function(r){
						if (r)
						{
							tempAppUl.children().remove();
							This.trashTempApp = [];
							$('#showTrashWin').window('close');
						}
					});
				});

				recoverButton.click(function(){
					var trashAppSelected = tempAppUl.find('.trashAppSelected');
					trashAppSelected.each(function(i){
						var tmpIndex = trashAppSelected.eq(i).attr('whichDesk');
						var desk = $('.aos_desk').eq(tmpIndex).find('.iconDesk ul');
						This.trashTempApp.pop(this);
						$(this).unbind('click').removeAttr('whichDesk').removeClass('trashAppSelected').find('img').css({'opacity':'', 'float':'', 'width':'', 'height':''});
						$(this).appendTo(desk);
					});
					$('.iconDesk li').draggable();
					$('.aos_trash').droppable();
				});
			});
		},

		raphaelClock : function(top, left)
		{
			if (this.mMousePos.left != left || this.mMousePos.top != top) return false;

			if ($('#aos_screen').find('svg').size() > 0) {
				$('#aos_screen').show();
				return false;
			}
			$('#aos_screen').width($(window).width()).height($(window).height()).css({'position':'absolute','top':'0px','left':'0px','z-index':'999999','background-color':'black'});
			$('#aos_screen').show().addClass('clockRunning');
			var r = Raphael("raphaelTimer", 600, 600),
                    R = 200,
                    init = true,
                    param = {stroke: "#fff", "stroke-width": 30},
                    hash = document.location.hash,
                    marksAttr = {fill: hash || "#444", stroke: "none"},
                    html = [
                        document.getElementById("h"),
                        document.getElementById("m"),
                        document.getElementById("s"),
                        document.getElementById("d"),
                        document.getElementById("mnth"),
                        document.getElementById("ampm")
                    ];
                // Custom Attribute
                r.customAttributes.arc = function (value, total, R) {
                    var alpha = 360 / total * value,
                        a = (90 - alpha) * Math.PI / 180,
                        x = 300 + R * Math.cos(a),
                        y = 300 - R * Math.sin(a),
                        color = "hsb(".concat(Math.round(R) / 200, ",", value / total, ", .75)"),
                        path;
                    if (total == value) {
                        path = [["M", 300, 300 - R], ["A", R, R, 0, 1, 1, 299.99, 300 - R]];
                    } else {
                        path = [["M", 300, 300 - R], ["A", R, R, 0, +(alpha > 180), 1, x, y]];
                    }
                    return {path: path, stroke: color};
                };

                drawMarks(R, 60);
                var sec = r.path().attr(param).attr({arc: [0, 60, R]});
                R -= 40;
                drawMarks(R, 60);
                var min = r.path().attr(param).attr({arc: [0, 60, R]});
                R -= 40;
                drawMarks(R, 12);
                var hor = r.path().attr(param).attr({arc: [0, 12, R]});
                R -= 40;
                drawMarks(R, 31);
                var day = r.path().attr(param).attr({arc: [0, 31, R]});
                R -= 40;
                drawMarks(R, 12);
                var mon = r.path().attr(param).attr({arc: [0, 12, R]});
                var pm = r.circle(300, 300, 16).attr({stroke: "none", fill: Raphael.hsb2rgb(15 / 200, 1, .75).hex});
                html[5].style.color = Raphael.hsb2rgb(15 / 200, 1, .75).hex;

                function updateVal(value, total, R, hand, id) {
                    if (total == 31) { // month
                        var d = new Date;
                        d.setDate(1);
                        d.setMonth(d.getMonth() + 1);
                        d.setDate(-1);
                        total = d.getDate();
                    }
                    var color = "hsb(".concat(Math.round(R) / 200, ",", value / total, ", .75)");
                    if (init) {
                        hand.animate({arc: [value, total, R]}, 900, ">");
                    } else {
                        if (!value || value == total) {
                            value = total;
                            hand.animate({arc: [value, total, R]}, 750, "bounce", function () {
                                hand.attr({arc: [0, total, R]});
                            });
                        } else {
                            hand.animate({arc: [value, total, R]}, 750, "elastic");
                        }
                    }
                    html[id].innerHTML = (value < 10 ? "0" : "") + value;
                    html[id].style.color = Raphael.getRGB(color).hex;
                }

                function drawMarks(R, total) {
                    if (total == 31) { // month
                        var d = new Date;
                        d.setDate(1);
                        d.setMonth(d.getMonth() + 1);
                        d.setDate(-1);
                        total = d.getDate();
                    }
                    var color = "hsb(".concat(Math.round(R) / 200, ", 1, .75)"),
                        out = r.set();
                    for (var value = 0; value < total; value++) {
                        var alpha = 360 / total * value,
                            a = (90 - alpha) * Math.PI / 180,
                            x = 300 + R * Math.cos(a),
                            y = 300 - R * Math.sin(a);
                        out.push(r.circle(x, y, 2).attr(marksAttr));
                    }
                    return out;
                }

                (function () {
                    var d = new Date,
                        am = (d.getHours() < 12),
                        h = d.getHours() % 12 || 12;
                    updateVal(d.getSeconds(), 60, 200, sec, 2);
                    updateVal(d.getMinutes(), 60, 160, min, 1);
                    updateVal(h, 12, 120, hor, 0);
                    updateVal(d.getDate(), 31, 80, day, 3);
                    updateVal(d.getMonth() + 1, 12, 40, mon, 4);
                    pm[(am ? "hide" : "show")]();
                    html[5].innerHTML = am ? "AM" : "PM";
                    setTimeout(arguments.callee, 1000);
                    init = false;
                })();
		},

		deleteIcon : function(dom)
		{
			$(dom).remove();
		},

		addWorkIcon : function(dom)
		{
			$('.window-shadow, .window-mask').remove();
			var imgSrc = $(dom).find('img').attr('src');
			var li = $('<li class="staticIcon"></li>').appendTo($('#workIcon'));
			dom.width(30).height(30);
			dom.appendTo(li);
			li.mouseover(function(e){
				$(this).removeClass('staticIcon').addClass('activeIcon');	
			}).mouseout(function(e){
				$(this).removeClass('activeIcon').addClass('staticIcon');	
			}).click(function(){
				$('#showAppWin').window('open');
			});
		},
	
		addWorkTrashIcon : function(dom)
		{
			$('.window-shadow, .window-mask').remove();
			var imgSrc = $(dom).find('img').attr('src');
			var li = $('<li class="staticIcon"></li>').appendTo($('#workIcon'));
			dom.width(30).height(30);
			dom.appendTo(li);
			li.mouseover(function(e){
				$(this).removeClass('staticIcon').addClass('activeIcon');	
			}).mouseout(function(e){
				$(this).removeClass('activeIcon').addClass('staticIcon');	
			}).click(function(){
				$('#showTrashWin').window('open');
			});
		},
	
		currentTime : function(){
			var d = new Date(),str = '';
		 	str += d.getFullYear()+'年';
		 	str += d.getMonth() + 1+'月';
		 	str += d.getDate()+'日';
		 	str += d.getHours()+'时';
		 	str += d.getMinutes()+'分';
			str += d.getSeconds()+'秒';
			return str;
		},
		
		changeIcon : function(e, el, a)
		{
			$(el).parent().animate({'width': a}, 300);
			$(el).animate({'width' : a}, 300);
			$(el).next().animate({'width' : a}, 300);
		},

		setArrow : function(e)
		{
			var distance = e.pageX;
			if (distance < 50)
			{
				$('.arrowLeft').show();
			}
			else if (distance > ($(window).width() - 50))
			{
				$('.arrowRight').show();
			}
			else
			{
				$('.arrowLeft, .arrowRight').hide();
			}
		},

		setDot : function()
		{
			var This = this;
			var len = This.getDesklen();
			var ww = 0;
			for (var i=0; i<len; i++)
			{
				var dot = $('<li class="dot20"></li>').appendTo($('.dotlist'));
				dot.click(function(){
					This.aosDeskindex = $(this).index();
				 	This.toggleDesk();
				}); 
				ww += dot.outerWidth(true);
			}
			return ww;
		},


		putIcon : function(icon)
		{
			var This = this;
			var title = icon.find('.title').text();
			var imgSrc = icon.find('img').attr('src');
			var tempSrc = $('.iconBar').find('img');
		
			for(var i=0; i<tempSrc.length; i++)
			{
				if ($(tempSrc[i]).attr('src') == imgSrc) 
					var flag = true;
			}


			if (!flag)
			{
				var div = $('<td class="iconList" valign="bottom"><div class="title">' + title + '</div><img style="width:70px"/></td>');
				div.find('img').attr('src', imgSrc);
				div.appendTo($('.iconBar'));
				this.setIconEvent();
			}
		},
		
		setDockIcon : function()
		{
			var This = this;
			var len = 8;
			for (var i=1; i<=8; i++)
			{
				var div = $('<td class="iconList" valign="bottom"><div class="title">list' + i + '</div><img style="width:70px"/></td>');
				div.find('img').attr('src', './image/icon' + i + '.png');
				div.appendTo($('.iconBar'));
			}

			$('.iconBar').droppable({
				accept : '.aos_icon',
				drop : function(e, ui)
				{
					This.putIcon($(ui.draggable));
				}
			}).sortable({placeholder: "ui-state-highlight"});;
			
			$('.iconBar').find('img').each(function(d){
				$(this).load(function(){
					if (d == len -1)
					{
						This.setIconEvent();
					}
				});
			});
		},
		
		setIconEvent : function(jq)
		{
			var This = this;
			$('.iconList >img').mouseover(function(e){
				$(this).parent().prev().show();
				This.changeIcon(e, this, 100);
			}).mouseout(function(e){
				$(this).parent().prev().hide();
				This.changeIcon(e, this, 70);
			}).reflect({
				height: 0.5,
				opacity: 0.3
			}).dblclick(function(e){
				var that = this;
				$('#showAppWin').window('open');
				$('#showAppWin').panel({
					onMinimize : function()
					{
						This.addWorkIcon($(that).clone());
					}
				});
			});
			//$('.iconBar').sortable({placeholder: "ui-state-highlight"});
		},

		setArea : function()
		{
			var left = $(window).width() - $('.aos_dock').width();
			$('.aos_dock').css({'left' : left / 2});
			$('#icongroup').width($(window).width());
			$('#aos_screen').hide();
			var ww = 0;
			if ($('.dotlist').children().size() == 0)
			{
				ww = this.setDot();
				$('.dotlist').find('li').eq(0).css('opacity','1');
			}
			else
			{
				ww = $('.dotlist').width();
			}
			var dotLeft = ($(window).width() - ww) / 2;
			$('.dotlist').css({'left' : dotLeft + 'px'}); 
			
			var arrowTop =  $(window).height() - $('.arrowLeft').height();
			$('.arrowLeft, .arrowRight').css({'top' : arrowTop / 2});            
		},

		setElement : function()
		{
			$('.aos_ctr').width($(window).width() * 6).height($(window).height());
			$('.aos_mask, .aos_desk').width($(window).width()).height($(window).height());
			$(".aos_ctr").children().each(function(i, u){
				var left = i*$(window).width();
				$(this).css({'left': left + 'px'});
			});
		},

		dragIcon : function()
		{
			$('.aos_dock_item').draggable();
			$('.aos_desk').droppable({
				drop: function(event, ui){
					e.stopPropagation();
				}
			});
		},

		getDesklen : function()
		{
			var len = $(".aos_ctr").children().length;
			return len;
		},

		toggleDesk : function()
		{
			var This = this;
			var len = This.getDesklen();
			var distance;
			
			var el = $(".aos_ctr").children().eq(This.aosDeskindex);
			if (This.aosDeskindex >= 0 && This.aosDeskindex<len)
			{
				distance = parseInt($(el).css('left'));
			}
			else
			{
				distance = 300;
			}
			
			$(".aos_ctr").children().each(function(){
				
				var oLeft = parseInt($(this).css('left'));

				if (This.aosDeskindex < 0)
				{
					oLeft += distance;
				}
				else
				{
					oLeft -= distance;
				}
				$(this).animate({'left' : oLeft}, 500, 'swing');
				
				if (This.aosDeskindex >= len)
				{
					oLeft += distance;
					$(this).animate({'left' : oLeft}, 300, 'swing');
				}
				if (This.aosDeskindex < 0)
				{

					oLeft -= distance;
					$(this).animate({'left' : oLeft}, 300, 'swing');
				}
			});
			This.aosDeskindex = This.aosDeskindex>=len ? len-1 : This.aosDeskindex;
			This.aosDeskindex = This.aosDeskindex<0 ? 0 : This.aosDeskindex;
			This.showDot(This.aosDeskindex);
		},

		showDot : function(i)
		{
			//console.log(i);
			$('.dotlist').find('li').css('opacity','0.4');
			$('.dotlist').find('li').eq(i).css('opacity','1');
		}




	}
})(AOS)
