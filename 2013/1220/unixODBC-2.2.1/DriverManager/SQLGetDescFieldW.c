/*********************************************************************
 *
 * This is based on code created by Peter Harvey,
 * (pharvey@codebydesign.com).
 *
 * Modified and extended by Nick Gorham
 * (nick@easysoft.com).
 *
 * Any bugs or problems should be considered the fault of Nick and not
 * Peter.
 * 
 * copyright (c) 1999 Nick Gorham
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 **********************************************************************
 *
 * $Id: SQLGetDescFieldW.c,v 1.1.1.1 2001/10/17 16:40:05 lurcher Exp $
 *
 * $Log: SQLGetDescFieldW.c,v $
 * Revision 1.1.1.1  2001/10/17 16:40:05  lurcher
 *
 * First upload to SourceForge
 *
 * Revision 1.4  2001/07/03 09:30:41  nick
 *
 * Add ability to alter size of displayed message in the log
 *
 * Revision 1.3  2001/04/17 16:29:39  nick
 *
 * More checks and autotest fixes
 *
 * Revision 1.2  2001/04/12 17:43:36  nick
 *
 * Change logging and added autotest to odbctest
 *
 * Revision 1.1  2000/12/31 20:30:54  nick
 *
 * Add UNICODE support
 *
 *
 **********************************************************************/

#include "drivermanager.h"

static char const rcsid[]= "$RCSfile: SQLGetDescFieldW.c,v $";

SQLRETURN SQLGetDescFieldW( SQLHDESC descriptor_handle,
           SQLSMALLINT rec_number, 
           SQLSMALLINT field_identifier,
           SQLPOINTER value, 
           SQLINTEGER buffer_length,
           SQLINTEGER *string_length )
{
    /*
     * not quite sure how the descriptor can be
     * allocated to a statement, all the documentation talks
     * about state transitions on statement states, but the
     * descriptor may be allocated with more than one statement
     * at one time. Which one should I check ?
     */
    DMHDESC descriptor = (DMHDESC) descriptor_handle;
    SQLRETURN ret;
    SQLCHAR s1[ 100 + LOG_MESSAGE_LEN ];

    /*
     * check descriptor
     */

    if ( !__validate_desc( descriptor ))
    {
        dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    "Error: SQL_INVALID_HANDLE" );

        return SQL_INVALID_HANDLE;
    }

    function_entry( descriptor );

    if ( log_info.log_flag )
    {
        sprintf( descriptor -> msg, "\n\t\tEntry:\
            \n\t\t\tDescriptor = %p\
            \n\t\t\tRec Number = %d\
            \n\t\t\tField Attr = %s\
            \n\t\t\tValue = %p\
            \n\t\t\tBuffer Length = %d\
            \n\t\t\tStrLen = %p",
                descriptor,
                rec_number,
                __desc_attr_as_string( s1, field_identifier ),
                value, 
                (int)buffer_length,
                (void*)string_length );

        dm_log_write( __FILE__, 
                __LINE__, 
                LOG_INFO, 
                LOG_INFO, 
                descriptor -> msg );
    }

    thread_protect( SQL_HANDLE_DESC, descriptor );

    if ( descriptor -> connection -> state < STATE_C4 )
    {
        dm_log_write( __FILE__, 
                __LINE__, 
                LOG_INFO, 
                LOG_INFO, 
                "Error: HY010" );

        __post_internal_error( &descriptor -> error,
                ERROR_HY010, NULL,
                descriptor -> connection -> environment -> requested_version );

        thread_release( SQL_HANDLE_DESC, descriptor );

        return function_return( descriptor, SQL_ERROR );
    }

    if ( descriptor -> connection -> unicode_driver )
    {
        if ( !CHECK_SQLGETDESCFIELDW( descriptor -> connection ))
        {
            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    "Error: IM001" );

            __post_internal_error( &descriptor -> error,
                    ERROR_IM001, NULL,
                    descriptor -> connection -> environment -> requested_version );

            thread_release( SQL_HANDLE_DESC, descriptor );

            return function_return( descriptor, SQL_ERROR );
        }

        ret = SQLGETDESCFIELDW( descriptor -> connection,
                descriptor -> driver_desc,
                rec_number, 
                field_identifier,
                value, 
                buffer_length,
                string_length );
    }
    else
    {
        if ( !CHECK_SQLGETDESCFIELD( descriptor -> connection ))
        {
            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    "Error: IM001" );

            __post_internal_error( &descriptor -> error,
                    ERROR_IM001, NULL,
                    descriptor -> connection -> environment -> requested_version );

            thread_release( SQL_HANDLE_DESC, descriptor );

            return function_return( descriptor, SQL_ERROR );
        }

        ret = SQLGETDESCFIELD( descriptor -> connection,
                descriptor -> driver_desc,
                rec_number, 
                field_identifier,
                value, 
                buffer_length,
                string_length );

        if ( SQL_SUCCEEDED( ret ) && value )
        {
            switch( field_identifier )
            {
              case SQL_DESC_BASE_COLUMN_NAME:
              case SQL_DESC_BASE_TABLE_NAME:
              case SQL_DESC_CATALOG_NAME:
              case SQL_DESC_LABEL:
              case SQL_DESC_LITERAL_PREFIX:
              case SQL_DESC_LITERAL_SUFFIX:
              case SQL_DESC_LOCAL_TYPE_NAME:
              case SQL_DESC_NAME:
              case SQL_DESC_SCHEMA_NAME:
              case SQL_DESC_TABLE_NAME:
              case SQL_DESC_TYPE_NAME:
                ansi_to_unicode_in_place( value, SQL_NTS );
                break;
            }
        }
    }

    if ( log_info.log_flag )
    {
        sprintf( descriptor -> msg, 
                "\n\t\tExit:[%s]",
                    __get_return_status( ret ));

        dm_log_write( __FILE__, 
                __LINE__, 
                LOG_INFO, 
                LOG_INFO, 
                descriptor -> msg );
    }

    thread_release( SQL_HANDLE_DESC, descriptor );

    return function_return( descriptor, ret );
}
