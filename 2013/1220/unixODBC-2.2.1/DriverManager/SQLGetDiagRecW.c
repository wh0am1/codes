/*********************************************************************
 *
 * This is based on code created by Peter Harvey,
 * (pharvey@codebydesign.com).
 *
 * Modified and extended by Nick Gorham
 * (nick@easysoft.com).
 *
 * Any bugs or problems should be considered the fault of Nick and not
 * Peter.
 *
 * copyright (c) 1999 Nick Gorham
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 **********************************************************************
 *
 * $Id: SQLGetDiagRecW.c,v 1.2 2001/12/13 13:00:32 lurcher Exp $
 *
 * $Log: SQLGetDiagRecW.c,v $
 * Revision 1.2  2001/12/13 13:00:32  lurcher
 *
 * Remove most if not all warnings on 64 bit platforms
 * Add support for new MS 3.52 64 bit changes
 * Add override to disable the stopping of tracing
 * Add MAX_ROWS support in postgres driver
 *
 * Revision 1.1.1.1  2001/10/17 16:40:05  lurcher
 *
 * First upload to SourceForge
 *
 * Revision 1.3  2001/07/03 09:30:41  nick
 *
 * Add ability to alter size of displayed message in the log
 *
 * Revision 1.2  2001/04/12 17:43:36  nick
 *
 * Change logging and added autotest to odbctest
 *
 * Revision 1.1  2000/12/31 20:30:54  nick
 *
 * Add UNICODE support
 *
 *
 **********************************************************************/

#include "drivermanager.h"

static char const rcsid[]= "$RCSfile: SQLGetDiagRecW.c,v $";

static SQLRETURN extract_sql_error_rec_w( EHEAD *head,
        SQLWCHAR *sqlstate,
        SQLINTEGER rec_number,
        SQLINTEGER *native_error,
        SQLWCHAR *message_text,
        SQLSMALLINT buffer_length,
        SQLSMALLINT *text_length )
{
    SQLRETURN ret;

    if ( sqlstate )
    {
        SQLWCHAR *tmp;

        tmp = ansi_to_unicode((SQLCHAR*) "00000", SQL_NTS );
        wide_strcpy( sqlstate, tmp );
        free( tmp );
    }

    if ( rec_number <= head -> sql_diag_head.internal_count )
    {
        ERROR *ptr;

        ptr = head -> sql_diag_head.internal_list_head;
        while( rec_number > 1 )
        {
            ptr = ptr -> next;
            rec_number --;
        }

        if ( sqlstate )
        {
            wide_strcpy( sqlstate, ptr -> sqlstate );
        }
        if ( buffer_length < wide_strlen( ptr -> msg ) + 1 )
        {
            ret = SQL_SUCCESS_WITH_INFO;
        }
        else
        {
            ret = SQL_SUCCESS;
        }

        if ( message_text )
        {
            if ( ret == SQL_SUCCESS )
            {
                wide_strcpy( message_text, ptr -> msg );
            }
            else
            {
                memcpy( message_text, ptr -> msg, buffer_length * 2 );
                message_text[ buffer_length - 1 ] = '\0';
            }
        }

        if ( text_length )
        {
            *text_length = wide_strlen( ptr -> msg );
        }

        if ( native_error )
        {
            *native_error = ptr -> native_error;
        }

        /*
         * map 3 to 2 if required
         */

        if ( SQL_SUCCEEDED( ret ) && sqlstate )
            __map_error_state( sqlstate, __get_version( head ));

        return ret;
    }
    else if ( rec_number <= head -> sql_diag_head.internal_count + 
            head -> sql_diag_head.error_count )
    {
        ERROR *ptr;
        rec_number -= head -> sql_diag_head.internal_count;

        if ( __get_connection( head ) -> unicode_driver &&
            CHECK_SQLGETDIAGRECW( __get_connection( head )))
        {
            ret = SQLGETDIAGRECW( __get_connection( head ),
                    head -> handle_type,
                    __get_driver_handle( head ),
                    rec_number,
                    sqlstate,
                    native_error,
                    message_text,
                    buffer_length,
                    text_length );

            /*
             * map 3 to 2 if required
             */

            if ( SQL_SUCCEEDED( ret ) && sqlstate )
            {
                __map_error_state_w( sqlstate, __get_version( head ));
            }

            return ret;
        }
        else if ( !__get_connection( head ) -> unicode_driver &&
            CHECK_SQLGETDIAGREC( __get_connection( head )))
        {
            ret = SQLGETDIAGREC( __get_connection( head ),
                    head -> handle_type,
                    __get_driver_handle( head ),
                    rec_number,
                    sqlstate,
                    native_error,
                    message_text,
                    buffer_length,
                    text_length );

            /*
             * map 3 to 2 if required
             */

            if ( SQL_SUCCEEDED( ret ) && sqlstate )
            {
                if ( sqlstate )
                {
                    ansi_to_unicode_in_place((SQLCHAR*) sqlstate, SQL_NTS );
                    __map_error_state_w( sqlstate, __get_version( head ));
                }
                if ( message_text )
                {
                    ansi_to_unicode_in_place((SQLCHAR*) message_text, SQL_NTS );
                }
            }

            return ret;
        }
        else
        {
            ptr = head -> sql_diag_head.error_list_head;
            while( rec_number > 1 )
            {
                ptr = ptr -> next;
                rec_number --;
            }

            if ( sqlstate )
            {
                wide_strcpy( sqlstate, ptr -> sqlstate );
            }
            if ( buffer_length < wide_strlen( ptr -> msg ) + 1 )
            {
                ret = SQL_SUCCESS_WITH_INFO;
            }
            else
            {
                ret = SQL_SUCCESS;
            }

            if ( message_text )
            {
                if ( ret == SQL_SUCCESS )
                {
                    wide_strcpy( message_text, ptr -> msg );
                }
                else
                {
                    memcpy( message_text, ptr -> msg, buffer_length * 2 );
                    message_text[ buffer_length - 1 ] = '\0';
                }
            }

            if ( text_length )
            {
                *text_length = wide_strlen( ptr -> msg );
            }

            if ( native_error )
            {
                *native_error = ptr -> native_error;
            }

            /*
             * map 3 to 2 if required
             */

            if ( SQL_SUCCEEDED( ret ) && sqlstate )
                __map_error_state_w( sqlstate, __get_version( head ));

            return ret;
        }
    }
    else
    {
        return SQL_NO_DATA;
    }
}

SQLRETURN SQLGetDiagRecW( SQLSMALLINT handle_type,
        SQLHANDLE   handle,
        SQLSMALLINT rec_number,
        SQLWCHAR     *sqlstate,
        SQLINTEGER  *native,
        SQLWCHAR     *message_text,
        SQLSMALLINT buffer_length,
        SQLSMALLINT *text_length_ptr )
{
    SQLRETURN ret;
    SQLCHAR s0[ 32 ], s1[ 100 + LOG_MESSAGE_LEN ];

    if ( rec_number < 1 )
    {
        return SQL_ERROR;
    }

    if ( handle_type == SQL_HANDLE_ENV )
    {
        DMHENV environment = ( DMHENV ) handle;

        if ( !__validate_env( environment ))
        {
            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    "Error: SQL_INVALID_HANDLE" );

            return SQL_INVALID_HANDLE;
        }

        thread_protect( SQL_HANDLE_ENV, environment );

        if ( log_info.log_flag )
        {
            sprintf( environment -> msg, 
                "\n\t\tEntry:\
                \n\t\t\tEnvironment = %p\
                \n\t\t\tRec Number = %d\
                \n\t\t\tSQLState = %p\
                \n\t\t\tNative = %p\
                \n\t\t\tMessage Text = %p\
                \n\t\t\tBuffer Length = %d\
                \n\t\t\tText Len Ptr = %p",
                    environment,
                    rec_number,
                    sqlstate,
                    native,
                    message_text,
                    buffer_length,
                    text_length_ptr );

            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    environment -> msg );
        }

        ret = extract_sql_error_rec_w( &environment -> error,
                sqlstate,
                rec_number,
                native,
                message_text,
                buffer_length,
                text_length_ptr );

        if ( log_info.log_flag )
        {
            if ( SQL_SUCCEEDED( ret ))
            {
                sprintf( environment -> msg, 
                    "\n\t\tExit:[%s]\
                    \n\t\t\tSQLState = %s\
                    \n\t\t\tNative = %s\
                    \n\t\t\tMessage Text = %s",
                        __get_return_status( ret ),
                        sqlstate,
                        __ptr_as_string( s0, native ),
                        __sdata_as_string( s1, SQL_CHAR, 
                            text_length_ptr, message_text ));
            }
            else
            {
                sprintf( environment -> msg, 
                    "\n\t\tExit:[%s]",
                        __get_return_status( ret ));
            }

            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    environment -> msg );
        }

        thread_release( SQL_HANDLE_ENV, environment );

        return ret;
    }
    else if ( handle_type == SQL_HANDLE_DBC )
    {
        DMHDBC connection = ( DMHDBC ) handle;

        if ( !__validate_dbc( connection ))
        {
            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    "Error: SQL_INVALID_HANDLE" );

            return SQL_INVALID_HANDLE;
        }

        thread_protect( SQL_HANDLE_DBC, connection );

        if ( log_info.log_flag )
        {
            sprintf( connection -> msg, 
                "\n\t\tEntry:\
                \n\t\t\tConnection = %p\
                \n\t\t\tRec Number = %d\
                \n\t\t\tSQLState = %p\
                \n\t\t\tNative = %p\
                \n\t\t\tMessage Text = %p\
                \n\t\t\tBuffer Length = %d\
                \n\t\t\tText Len Ptr = %p",
                    connection,
                    rec_number,
                    sqlstate,
                    native,
                    message_text,
                    buffer_length,
                    text_length_ptr );

            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    connection -> msg );
        }

        ret = extract_sql_error_rec_w( &connection -> error,
                sqlstate,
                rec_number,
                native,
                message_text,
                buffer_length,
                text_length_ptr );

        if ( log_info.log_flag )
        {
            if ( SQL_SUCCEEDED( ret ))
            {
                sprintf( connection -> msg, 
                    "\n\t\tExit:[%s]\
                    \n\t\t\tSQLState = %s\
                    \n\t\t\tNative = %s\
                    \n\t\t\tMessage Text = %s",
                        __get_return_status( ret ),
                        sqlstate,
                        __ptr_as_string( s0, native ),
                        __sdata_as_string( s1, SQL_CHAR, 
                            text_length_ptr, message_text ));
            }
            else
            {
                sprintf( connection -> msg, 
                    "\n\t\tExit:[%s]",
                        __get_return_status( ret ));
            }

            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    connection -> msg );
        }

        thread_release( SQL_HANDLE_DBC, connection );

        return ret;
    }
    else if ( handle_type == SQL_HANDLE_STMT )
    {
        DMHSTMT statement = ( DMHSTMT ) handle;

        if ( !__validate_stmt( statement ))
        {
            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    "Error: SQL_INVALID_HANDLE" );

            return SQL_INVALID_HANDLE;
        }

        thread_protect( SQL_HANDLE_STMT, statement );

        if ( log_info.log_flag )
        {
            sprintf( statement -> msg, 
                "\n\t\tEntry:\
                \n\t\t\tStatement = %p\
                \n\t\t\tRec Number = %d\
                \n\t\t\tSQLState = %p\
                \n\t\t\tNative = %p\
                \n\t\t\tMessage Text = %p\
                \n\t\t\tBuffer Length = %d\
                \n\t\t\tText Len Ptr = %p",
                    statement,
                    rec_number,
                    sqlstate,
                    native,
                    message_text,
                    buffer_length,
                    text_length_ptr );

            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    statement -> msg );
        }

        ret = extract_sql_error_rec_w( &statement -> error,
                sqlstate,
                rec_number,
                native,
                message_text,
                buffer_length,
                text_length_ptr );

        if ( log_info.log_flag )
        {
            if ( SQL_SUCCEEDED( ret ))
            {
                sprintf( statement -> msg, 
                    "\n\t\tExit:[%s]\
                    \n\t\t\tSQLState = %s\
                    \n\t\t\tNative = %s\
                    \n\t\t\tMessage Text = %s",
                        __get_return_status( ret ),
                        sqlstate,
                        __ptr_as_string( s0, native ),
                        __sdata_as_string( s1, SQL_CHAR, 
                            text_length_ptr, message_text ));
            }
            else
            {
                sprintf( statement -> msg, 
                    "\n\t\tExit:[%s]",
                        __get_return_status( ret ));
            }

            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    statement -> msg );
        }

        thread_release( SQL_HANDLE_STMT, statement );

        return ret;
    }
    else if ( handle_type == SQL_HANDLE_DESC )
    {
        DMHDESC descriptor = ( DMHDESC ) handle;

        if ( !__validate_desc( descriptor ))
        {
            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    "Error: SQL_INVALID_HANDLE" );

            return SQL_INVALID_HANDLE;
        }

        thread_protect( SQL_HANDLE_DESC, descriptor );

        if ( log_info.log_flag )
        {
            sprintf( descriptor -> msg, 
                "\n\t\tEntry:\
                \n\t\t\tDescriptor = %p\
                \n\t\t\tRec Number = %d\
                \n\t\t\tSQLState = %p\
                \n\t\t\tNative = %p\
                \n\t\t\tMessage Text = %p\
                \n\t\t\tBuffer Length = %d\
                \n\t\t\tText Len Ptr = %p",
                    descriptor,
                    rec_number,
                    sqlstate,
                    native,
                    message_text,
                    buffer_length,
                    text_length_ptr );

            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    descriptor -> msg );
        }

        ret = extract_sql_error_rec_w( &descriptor -> error,
                sqlstate,
                rec_number,
                native,
                message_text,
                buffer_length,
                text_length_ptr );

        if ( log_info.log_flag )
        {
            if ( SQL_SUCCEEDED( ret ))
            {
                sprintf( descriptor -> msg, 
                    "\n\t\tExit:[%s]\
                    \n\t\t\tSQLState = %s\
                    \n\t\t\tNative = %s\
                    \n\t\t\tMessage Text = %s",
                        __get_return_status( ret ),
                        sqlstate,
                        __ptr_as_string( s0, native ),
                        __sdata_as_string( s1, SQL_CHAR, 
                            text_length_ptr, message_text ));
            }
            else
            {
                sprintf( descriptor -> msg, 
                    "\n\t\tExit:[%s]",
                        __get_return_status( ret ));
            }

            dm_log_write( __FILE__, 
                    __LINE__, 
                    LOG_INFO, 
                    LOG_INFO, 
                    descriptor -> msg );
        }

        thread_release( SQL_HANDLE_DESC, descriptor );

        return ret;
    }
    return SQL_NO_DATA;
}

