/**********************************************************************************
 * iniOpen
 *
 *
 **************************************************
 * This code was created by Peter Harvey @ CodeByDesign.
 * Released under LGPL 28.JAN.99
 *
 * Contributions from...
 * -----------------------------------------------
 * PAH = Peter Harvey		- pharvey@codebydesign.com
 * -----------------------------------------------
 *
 * PAH	06.MAR.99	Can now create file-less INI. Pass NULL for
 *					pszFileName. Then copy a file name into hIni->szFileName
 *					before calling iniCommit.
 **************************************************/

#include "ini.h"

int iniOpen( HINI *hIni, char *pszFileName, char cComment, char cLeftBracket, char cRightBracket, char cEqual, int bCreate )
{
    FILE    *hFile;
    char    szLine[INI_MAX_LINE+1];
    char    szObjectName[INI_MAX_OBJECT_NAME+1];
    char    szPropertyName[INI_MAX_PROPERTY_NAME+1];
    char    szPropertyValue[INI_MAX_PROPERTY_VALUE+1];
    int     nValidFile;

    /* INIT STATEMENT */
    *hIni = malloc( sizeof(INI) );
    if ( pszFileName && pszFileName != STDINFILE )
        strncpy((*hIni)->szFileName, pszFileName, ODBC_FILENAME_MAX );
    else if ( pszFileName == STDINFILE )
        strncpy((*hIni)->szFileName, "stdin", ODBC_FILENAME_MAX );
    else
        strncpy((*hIni)->szFileName, "", ODBC_FILENAME_MAX );

    (*hIni)->cComment           = cComment;
    (*hIni)->cLeftBracket       = cLeftBracket;
    (*hIni)->cRightBracket      = cRightBracket;
    (*hIni)->cEqual             = cEqual;
    (*hIni)->bChanged           = FALSE;
    (*hIni)->hCurObject         = NULL;
    (*hIni)->hFirstObject       = NULL;
    (*hIni)->hLastObject        = NULL;
    (*hIni)->nObjects           = 0;
    (*hIni)->bReadOnly          = 0;

    /* OPEN FILE */
    if ( pszFileName )
    {
        if ( pszFileName == STDINFILE )
        {
            hFile = stdin;
        }
        else
        {
            hFile = fopen( pszFileName, "r" );
        }

        if ( !hFile )
        {
            if ( bCreate == TRUE )
            {
                hFile = fopen( pszFileName, "w" );
            }
        }

        if ( !hFile )
        {
            free( *hIni );
            *hIni = NULL;
            return INI_ERROR;
        }

        nValidFile = _iniScanUntilObject( *hIni, hFile, szLine );
        if ( nValidFile == INI_SUCCESS )
        {
            char *ptr;
            do
            {
                if ( szLine[0] == cLeftBracket )
                {
                    _iniObjectRead( (*hIni), szLine, szObjectName );
                    iniObjectInsert( (*hIni), szObjectName );
                }
                else if ( (szLine[0] != cComment) && !isspace(szLine[0]) )
                {
                    _iniPropertyRead( (*hIni), szLine, szPropertyName, szPropertyValue );
                    iniPropertyInsert( (*hIni), szPropertyName, szPropertyValue );
                }

            } while ( (ptr = fgets( szLine, INI_MAX_LINE, hFile )) != NULL );
        }
        else if ( nValidFile == INI_ERROR )
        {
            /* INVALID FILE */
            if ( hFile != NULL )
                fclose( hFile );
            free( *hIni );
            *hIni = NULL;
            return INI_ERROR;
        }

        /* CLEANUP */
        if ( hFile != NULL )
            fclose( hFile );

        iniObjectFirst( *hIni );

    } /* if file given */

    return INI_SUCCESS;
}


