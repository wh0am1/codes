***************************************************************
* All programs are GPL.                                       *
* All libs are LGPL (except nn which is GPL?).                *
*							      *
* unixODBC is considered to be a single piece of software     *
* made from many components.                                  *
*                                                             *
***************************************************************

+-------------------------------------------------------------+
| unixODBC                                                    |
+-------------------------------------------------------------+

PURPOSE:

To provide a complete ODBC solution for the Linux platform.


COMPONENTS:

1.  libodbc.so		(ODBC Driver Manager)
2.  ODBCConfig 		(GUI Setup using libodbcinst.so)
3.  libodbcinst.so	(ODBC Installer/Setup)
4.  odbcinst		(cmd line UI for libodbcinst.so)
5.  libini.a 		(ini file handler)
6.  liblog.a		(log handler)
7.  libodbcdrvcfg1S.so	(GUI config for server based sources... basic)
8.  libodbcdrvcfg2S.so	(GUI config for file based sources... basic)
9.  liboplodbcS.so	(GUI config for OpenLink's opl driver)
10. libodbcminiS.so	(GUI config for MiniSQL driver)
11. libodbcmyS.so	(GUI config for MySQL driver)
12. libodbcpgsqlS.so	(GUI config for PostgreSQL driver)
13. libodbcmini.so	(driver for MiniSQL)
14. libodbcmy.so	(driver for MySQL)
15. libodbcpgsql.so	(driver for PostgreSQL)
16. libodbctxt.so	(driver for Text files)
17. dltest		(simple cmd line tool)
18. isql		(cmd line tool... batch & interactive SQL)
19. Driver Template	(a template for Driver programmers)
20. DataManager		(GUI to explore ODBC Data Sources)
21. liblst.so		(list handler)

ISSUES:

1. DRIVERS DRIVERS DRIVERS... time to get serious with the high-end commercial DBMS's.
2. odbcinst needs some work to better handle special sections [ODBC Data Sources] and [ODBC Drivers]
3. None of the Driver Config libs implement the validation process.
5. DataManager needs to handle more database object types (a lot of very usefull potential here)
6. positional updates/deletes (do it in the DM so that all Drivers have it)
7. need compliance test program

REQUIREMENTS:

1. Drivers require their respective libs. Go into Drivers/Makefile to comment/uncomment
drivers you want to build and then go into Drivers/DriverName/Config.mk to ensure that
the supporting libs and includes can be found.

2. GUI stuff requires Qt 2.0. The GUI code (ODBCConfig & DataManager) have been 
upgraded to Qt 2.0. You can get a copy of Qt 2.0 at http://www.troll.no...
or go to ftp://ftp.troll.no/qt/snapshots/.

3. I use egcs to compile the GUI stuff. Non-GUI stuff should compile with just about
anything as that code is pretty much standard stuff.


SETUP:

This setup procedure is not the nicest. More work will be
put into this in the future.

1. gunzip unixODBC*.gz
2. tar xvf unixODBC*.tar
3. cd unixODBC*
4. ./configure (options)
5. make
6. make install
7. register one or more drivers using ODBCConfig or odbcinst (must be root)
8. create one or more DSN's using ODBCConfig (must be root for System DSN)

The best idea is to log in as root and make with the defaults. This
places the programs in /usr/bin, the includes in /usr/include and
the libs in /usr/libs. This usually avoids missing refs and libs
because /usr/lib should be in your lib path already.

But if you find that some libs can not be found you can;

a) create links ie /usr/lib to */LibDir/libInQuestion
b) add */LibDir to your LD_LIBRARY_PATH
c) copy the */libInQuestion to ie /usr/lib


+-------------------------------------------------------------+
| Peter Harvey                                                |
| pharvey@codebydesign.com                                    |
| http://www.unixodbc.org                                     |
| mailing list: unixodbc-dev@unixodbc.org                     |
| 16.MAY.99                                                   |
+-------------------------------------------------------------+

